package main

import (
	"encoding/json"
	"testing"

	"github.com/buger/jsonparser"
	jsoniter "github.com/json-iterator/go"
)

/*
encoding/json
*/
func BenchmarkDecodeStdStructMedium(b *testing.B) {
	b.ReportAllocs()
	var data MediumPayload
	for i := 0; i < b.N; i++ {
		json.Unmarshal(mediumFixture, &data)
	}
}

// jsoniter
func BenchmarkDecodeJsoniterStructMedium(b *testing.B) {
	b.ReportAllocs()
	var data MediumPayload
	for i := 0; i < b.N; i++ {
		jsoniter.Unmarshal(mediumFixture, &data)
	}
}

// jsonParser
func BenchmarkDecodeJsonParserMedium(b *testing.B) {
	b.ReportAllocs()
	paths := [][]string{
		{"person", "name", "fullName"},
		{"person", "github", "followers"},
	}
	for i := 0; i < b.N; i++ {
		var data = MediumPayload{
			Person: &CBPerson{
				Name:   &CBName{},
				Github: &CBGithub{},
			},
		}
		jsonparser.EachKey(mediumFixture, func(idx int, value []byte, vt jsonparser.ValueType, err error) {
			switch idx {
			case 0:
				data.Person.Name.FullName, _ = jsonparser.ParseString(value)
			case 1:
				v, _ := jsonparser.ParseInt(value)
				data.Person.Github.Followers = int(v)
			}
		}, paths...)
	}
}
